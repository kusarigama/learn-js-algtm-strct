const arr = [1, 22, 3, 2, 4, 6, 5, 8, 30, 22, 90, 19, 59, 9, 10, 11, 7, 16, 14, 15, 13, -17, -18];
let count = 0;

function selectionSort(array) {
  for(let i = 0; i < array.length; i++) {
    let indexMin = i;
    for(let j = i + 1; j < array.length; j++ ) {
      if (array[j] < array[indexMin]) {
        indexMin = j;
      }
      count += 1;
    }
    let tmp = array[i];
    array[i] = array[indexMin];
    array[indexMin] = tmp;
  }
  return array;
}

// O(n^2)
console.log(selectionSort(arr));
console.log(arr.length);
console.log('count:' + count);