function fact1(n) {
  if(n == 0 || n == 1) {
    return 1;
  }
  return n * fact1(n - 1);
}

function fact2(n) {

  const st = [[n, 1]];

  while (st.length > 0) {
    const [curr, result] = st.pop();

    if(curr === 0 || curr === 1) {
      return result;
    }
    st.push([curr - 1, result * curr]);
  }
}

//O(n)
console.log(fact2(12));